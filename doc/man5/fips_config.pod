=pod

=head1 NAME

fips_config - OpenSSL FIPS configuration

=head1 DESCRIPTION

This command is disabled in Red Hat Enterprise Linux. The FIPS provider is
automatically loaded when the system is booted in FIPS mode, or when the
environment variable B<OPENSSL_FORCE_FIPS_MODE> is set. See the documentation
for more information.

=head1 COPYRIGHT

Copyright 2019-2021 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the Apache License 2.0 (the "License").  You may not use
this file except in compliance with the License.  You can obtain a copy
in the file LICENSE in the source distribution or at
L<https://www.openssl.org/source/license.html>.

=cut
